import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, HeaderTitle} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Login from '../screen/login';
import SecendPage from '../screen/secendPage';
import Search from '../screen/search';
import InfoDevice from '../screen/infoDevice';
import Profile from '../screen/profile';
import ProfileList from '../screen/profilelist';
import FirstInfo from '../screen/firstInfo';
import Seting from '../screen/setting';

const AuthStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Stock = () => {
  return (
    <AuthStack.Navigator>
           <AuthStack.Screen
        name="Search"
        component={Search}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />
            <AuthStack.Screen
        name="FirstInfo"
        component={FirstInfo}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />
     <AuthStack.Screen
        name="Login"
        component={Login}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />





      
      <AuthStack.Screen
        name="Seting"
        component={Seting}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />

 

 
      <AuthStack.Screen
        name="InfoDevice"
        component={InfoDevice}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />

      <AuthStack.Screen
        name="ProfileList"
        component={ProfileList}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />

      <AuthStack.Screen
        name="Profile"
        component={Profile}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />

      <AuthStack.Screen
        name="SecendPage"
        component={Route}
        options={(navigation) => {
          return {
            headerShown: false,
          };
        }}
      />
    </AuthStack.Navigator>
  );
};

const Route = () => {
  return (
    <Drawer.Navigator drawerPosition="right">
      {/* <Drawer.Screen name="Login" component={Login} /> */}
      <Drawer.Screen name="SecendPage" component={SecendPage} />
    </Drawer.Navigator>
  );
};

export {Stock};
