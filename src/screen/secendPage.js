import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Idcard from 'react-native-vector-icons/AntDesign';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Icon from 'react-native-vector-icons/FontAwesome';
import NetInfo from '@react-native-community/netinfo';
import HeaderComponent from '../component/headerComponent';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const SecendPage = (props) => {



  return (
    <View style={[styles.container]}>
      {/* <HeaderComponent colorBackIcon={'#1879a4'}/> */}
      <View style={{width:'100%',height:10,backgroundColor:'#dce3ed'}}>
          <View style={{width:'20%',height:10,backgroundColor:colors.blue}}></View>
      </View>
        <ActivityIndicator color="#1879a4" size='large' style={{marginTop:HEIGHT/3}}/>       

  
 
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightwhite,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.navyblue,
    fontSize: 16,
  },
  box: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: '8%',
    width: '95%',
    height:'100%',
    alignSelf: 'center',
  },
  register: {
    // backgroundColor:'red',

    marginTop: '5%',
    alignSelf: 'center',
    width: '65%',
  },

  box1: {
    padding: 20,
    backgroundColor: colors.green,
    shadowColor: '#c5eff4',
    shadowOpacity: 0.8,
    shadowRadius: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },

    width: 130,
    height: 130,
    elevation: 5,
    borderRadius: 100,
    // borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    width: '95%',
    marginTop: '5%',
  },
});

export default SecendPage;
