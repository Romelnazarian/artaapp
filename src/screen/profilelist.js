import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import allStyle from '../utils/allStyle';
import {validate} from '../utils/validation';
import Arrow from 'react-native-vector-icons/MaterialIcons';
import Flag from 'react-native-vector-icons/Feather';
import Notti from 'react-native-vector-icons/Ionicons';
import Setting from 'react-native-vector-icons/AntDesign';
import HeaderComponent from '../component/headerComponent';
import { shadow } from 'react-native-paper';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const ProfileList = (props) => {
  const data = [
    {
      id: 1,
      title: 'مانومتر',
      code: '224455',
    },
    {
      id: 2,
      title: 'مانومتر',
      code: '224455',
    },
    {
      id: 3,
      title: 'مانومتر',
      code: '224455',
    },
    {
      id: 4,
      title: 'مانومتر',
      code: '224455',
    },
  ];
  const renderitem = (item) => {
    return (
      <View
        style={styles.wraper}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View
            style={styles.flagtotal}>
                <View style={styles.flag}>
                 <Flag name='flag' size={25} color={colors.darkgreenblue}/>
                </View>
            </View>
          <View style={{flexDirection: 'row', marginTop: '3%'}}>
            <View style={{paddingRight: '3%'}}>
              <View>
                <Text
                  style={[styles.text, {color: colors.black, fontSize: 22}]}>
                  مانومتر
                </Text>
                <Text style={[styles.text]}>کد دستگاه : 224455</Text>
              </View>
              <Text style={[styles.text]}>وضعیت : فعال</Text>
            </View>
            <View
              style={styles.image}>
              <Image
                source={require('../images/profile.png')}
                style={{width: '100%', height: '100%'}}
              />
            </View>
          </View>
        </View>
        <View
          style={styles.arrow}>
              <Arrow name='keyboard-arrow-down' size={30} color={colors.white}/>
          </View>
      </View>
    );
  };

  return (
    <View style={[styles.container]}>
      <HeaderComponent colorBackIcon={'#1879a4'} />
      <FlatList
        data={data}
        renderItem={({item}) => renderitem(item)}
        keyExtractor={(item) => item.id}
      />
        
        <View
            style={{
              marginTop: '5%',
              height: '8%',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity style={styles.icon}>
              <Setting name="setting" color={colors.darkgreenblue} size={25} />
            </TouchableOpacity>
            <ButtonComponent
              buttonStyle={{
                borderRadius: 10,
                backgroundColor: colors.white,
                borderColor: colors.blue,
                alignSelf: 'center',
                width: '70%',
              }}
              onClick={() => btn()}
              titleStyle={{color: colors.blue,fontSize: 18}}
              title={'مشاهده لیست و تجهیزات '}
            />
            <TouchableOpacity
              style={[
                styles.icon,
                {borderTopLeftRadius: 30, borderTopRightRadius: 0},
              ]}>
              <Notti
                name="ios-notifications-outline"
                color={colors.darkgreenblue}
                size={25}
              />
            </TouchableOpacity>
          </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightwhite,
    flex: 1,
  },
   wraper:{
    width: '90%',
    height: 120,
    backgroundColor: colors.white,
    alignSelf: 'center',
    marginTop: '5%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
   },
  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.darkGray,
    fontSize: 14,
  },
  box: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: '8%',
    width: '95%',
    height: '100%',
    alignSelf: 'center',
  },
  flagtotal:{
    borderRightWidth:1,
    borderColor:colors.blue,
    width: '12%',
     height:'100%',
  },
  flag:{
    justifyContent:"center",
    alignItems:'center',
    height:'50%',
    borderTopLeftRadius:20,
    borderBottomWidth:1,
    borderColor:colors.blue
  },
  image:{
    width: 80,
    height: 80,
    borderRadius: 100,
  },
  arrow:{
    width: '100%',
    height: '20%',
    backgroundColor: colors.blue,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    justifyContent:'center',
    alignItems:'center'
  },
  icon: {
    borderTopRightRadius: 30,
    width: 50,
    height: 50,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
    
  },
});

export default ProfileList;
