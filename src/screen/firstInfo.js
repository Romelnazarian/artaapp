import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Inbox from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import Notti from 'react-native-vector-icons/Ionicons';
import Setting from 'react-native-vector-icons/AntDesign';
import Arrow from 'react-native-vector-icons/MaterialIcons';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const FirstInfo = (props) => {
  const btn = () => {};
  return (
    <View style={[styles.container]}>
            <ScrollView keyboardShouldPersistTaps={'handled'}>

      <View
        style={{
          width: '100%',
          height: HEIGHT / 3,
          backgroundColor: colors.white,
        }}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#f47749', '#f15f60']}
          style={{height: HEIGHT / 3.2, borderBottomRightRadius: 50}}>
          <View style={{flexDirection: 'row',justifyContent:'space-around',paddingRight:'4%'}}>
            <Text style={[styles.text,{fontSize:25}]}>شرکت CCO</Text>
            <View
              style={styles.profile}>
                <View style={styles.profileImage}>
                  <Image source={require('../images/profile.png')} style={{width:'100%',height:'100%'}}/>
                </View>
                <Text style={[styles.text,{fontSize:20}]}>امیر رضا جولانی</Text>
              </View>
          </View>
        </LinearGradient>
      </View>
      <View style={{width:'90%',height:HEIGHT/4,backgroundColor:colors.white,alignSelf:'center',marginTop:'-12%',borderRadius:10}}>
           <Text style={[styles.text,{fontSize:22,paddingRight:'5%',}]}>اطلاعات اولیه کارشناس</Text>
           <View style={{flexDirection:'row',justifyContent:'space-around',marginTop:'2%'}}>
           <Text style={[styles.title,]}>  کد پرسنلی : 224455</Text>
           <Text style={[styles.title,]}>  کد ملی : 224455</Text>
           </View>
           <Text style={[styles.title,{paddingRight:'5%',marginTop:'2%'}]}>سمت : رییس بخش عمل جراحی</Text>
           <View style={{alignItems:'center',alignSelf:'center',width:'30%',flexDirection:'row',justifyContent:'space-around',marginTop:'2%'}}>
           <Text style={[styles.title,{color:colors.blue,fontSize:20}]}>دسترسی</Text>
            <Inbox name='inbox' size={25} color={colors.darkgreenblue}/>
           </View>

      </View>
      <View style={styles.line}></View>
      <ButtonComponent
            buttonStyle={{
            
              borderRadius: 10,
              backgroundColor: colors.blue,
              borderColor: colors.blue,
              alignSelf: 'center',
              height:HEIGHT/6
            }}
            onClick={() => btn()}
            titleStyle={{fontSize: 16}}
            titleStyle={{color: colors.white}}
            title={' تعمیرات '}
          />
                <ButtonComponent
            buttonStyle={{
              borderRadius: 10,
              backgroundColor: colors.blue,
              borderColor: colors.blue,
              alignSelf: 'center',
              height:HEIGHT/6
            }}
            onClick={() => btn()}
            titleStyle={{fontSize: 16}}
            titleStyle={{color: colors.white}}
            title={' کالیبراسیون '}
          />
       
          </ScrollView>
          <View
            style={{
              marginTop: '5%',
              height: '6.5%',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity style={styles.icon}>
              <Setting name="setting" color={colors.darkgreenblue} size={25} />
            </TouchableOpacity>
            <Arrow name="keyboard-arrow-up" size={35} color={colors.blue} style={{position:'absolute',left:WIDTH/2.2,top:'-50%'}}/>
           <View style={styles.not}>
             <Text style={{color:colors.white}}>2</Text>
           </View>
            <ButtonComponent
              buttonStyle={{
                backgroundColor: colors.white,
                borderColor: colors.white,
                alignSelf: 'center',
                width: '40%',
                height:50,
                borderRadius:20,
                marginTop:'10%'
              }}
              onClick={() => btn()}
              titleStyle={{color: colors.blue,fontSize: 14,marginTop:'-5%'}}
              title={'لیست تجهیزات'}
            />
            <TouchableOpacity
              style={[
                styles.icon,
                {borderTopLeftRadius: 30,
                   borderTopRightRadius: 0},
              ]}>
              <Notti
                name="ios-notifications-outline"
                color={colors.darkgreenblue}
                size={25}
              />
            </TouchableOpacity>
          </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightwhite,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.blue,
    fontSize: 16,
    paddingRight: '8%',
    marginTop: '5%',
  },
  box: {
    backgroundColor: colors.lightwhite,
    marginTop: '12%',
    width: '95%',
    alignSelf: 'center',
  },
  profile: {
    width: '40%',
    height: HEIGHT / 4,
    alignItems:'center',
    marginTop:'3%'
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 100,
  },
  line: {
    width: '100%',
    height: 2,
    backgroundColor: colors.blue,
    marginTop: '5%',
  },
  icon: {
    borderTopRightRadius: 30,
    width: 50,
    height: 50,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    
    elevation: 10,
  },
  not:{
    position:'absolute',
    left:WIDTH/1.5,
    top:'-20%',
    width:25,
    height:25,
    backgroundColor:colors.red,
    borderRadius:100,
    justifyContent:'center',
    alignItems:'center',
    zIndex:5
  }
});

export default FirstInfo;
