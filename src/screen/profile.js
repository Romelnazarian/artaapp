import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Notti from 'react-native-vector-icons/Ionicons';
import Setting from 'react-native-vector-icons/AntDesign';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const Profile = (props) => {
  const btn = () => {};
  return (
    <View style={[styles.container]}>
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={styles.box}>
          <View style={styles.profileImage}>
              <Image source={require('../images/profile.png')} style={{width:'100%',height:'100%'}}/>
          </View>
          <View style={styles.profile}>
            <View>
              <Text
                style={[
                  styles.text,
                  {
                    paddingLeft: 20,
                    marginTop: 0,
                    paddingRight: 0,
                    color: colors.blue,
                    fontSize: 28,
                    textAlign: 'left',
                  },
                ]}>
                شرکت cco
              </Text>
            </View>
          </View>
          <Text style={[styles.text, {color: colors.blue, fontSize: 22}]}>
            امیررضا جولانی
          </Text>
          <Text style={[styles.text, {color: colors.darkGray, fontSize: 20}]}>
            کد ملی : 22448899
          </Text>
          <Text style={[styles.text, {color: colors.darkGray, fontSize: 20}]}>
            کد پرسنلی : 22448899
          </Text>
          <Text style={[styles.text, {color: colors.darkGray, fontSize: 20}]}>
            سمت : رییس بخش عمل جراحی
          </Text>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text
              style={[
                styles.text,
                {paddingRight: 0, color: colors.blue, fontSize: 25},
              ]}>
              دسترسی
            </Text>
          </View>
          <Text style={[styles.text, {color: colors.darkGray, fontSize: 20}]}>
            بخش : دسترسی به بخش اتاق عمل
          </Text>
          <Text style={[styles.text, {color: colors.darkGray, fontSize: 20}]}>
            اقدام : تعمییر
          </Text>
          <View style={styles.line}></View>
          <ButtonComponent
            buttonStyle={{
              borderRadius: 10,
              backgroundColor: colors.blue,
              borderColor: colors.blue,
              alignSelf: 'center',
            }}
            onClick={() => btn()}
            titleStyle={{fontSize: 16}}
            titleStyle={{color: colors.white}}
            title={'تعمیرات'}
          />
          <ButtonComponent
            buttonStyle={{
              borderRadius: 10,
              backgroundColor: colors.blue,
              borderColor: colors.blue,
              alignSelf: 'center',
            }}
            onClick={() => btn()}
            titleStyle={{fontSize: 16}}
            titleStyle={{color: colors.white}}
            title={' کالیبراسیون '}
          />

          <View
            style={{
              marginTop: '5%',
              height: '6.5%',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity style={styles.icon}>
              <Setting name="setting" color={colors.darkgreenblue} size={25} />
            </TouchableOpacity>
            <ButtonComponent
              buttonStyle={{
                borderRadius: 10,
                backgroundColor: colors.white,
                borderColor: colors.blue,
                alignSelf: 'center',
                width: '70%',
              }}
              onClick={() => btn()}
              titleStyle={{fontSize: 16}}
              titleStyle={{color: colors.blue}}
              title={' کالیبراسیون '}
            />
            <TouchableOpacity
              style={[
                styles.icon,
                {borderTopLeftRadius: 30, borderTopRightRadius: 0},
              ]}>
              <Notti
                name="ios-notifications-outline"
                color={colors.darkgreenblue}
                size={25}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.navyblue,
    fontSize: 16,
    paddingRight: '8%',
    marginTop: '5%',
  },
  box: {
    backgroundColor: colors.lightwhite,
    marginTop: '12%',
    width: '95%',
    alignSelf: 'center',
  },
  profile: {
    width: '100%',
    height: HEIGHT / 6,
    // backgroundColor: 'red',
    flexDirection: 'row',
    position: 'relative',
  },
  profileImage: {
    width: 120,
    height: 120,
    backgroundColor: colors.red,
    borderRadius: 100,
    position: 'absolute',
    right: 20,
    top: '-5%',
  },
  line: {
    width: '100%',
    height: 2,
    backgroundColor: colors.blue,
    marginTop: '3%',
  },
  icon: {
    borderTopRightRadius: 30,
    width: 50,
    height: 50,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Profile;
