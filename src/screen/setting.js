import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
  Switch,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import ToggleSwitch from 'toggle-switch-react-native';
import Notti from 'react-native-vector-icons/Ionicons';
import Setting from 'react-native-vector-icons/AntDesign';
const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const Seting = (props) => {
  const btn = () => {};
  const [isEnabled, setIsEnabled] = useState(false);
  const [isEnabled2, setIsEnabled2] = useState(false);

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const toggleSwitch2 = () => setIsEnabled2((previousState2) => !previousState2);

  return (
    <View style={styles.container}>
                  <ButtonComponent
            buttonStyle={{
              borderRadius: 10,
              backgroundColor: colors.blue,
              borderColor: colors.blue,
              alignSelf: 'center',
            }}
            onClick={() => btn()}
            titleStyle={{fontSize: 16}}
            titleStyle={{color: colors.white}}
            title={'تنظیمات'}
          />
      <View style={styles.box}>
        <View style={styles.taggle}>
          <ToggleSwitch
            isOn={isEnabled}
            onColor={colors.blue}
            offColor={colors.white}
            labelStyle={{color: 'black'}}
            size="medium"
            onToggle={() => toggleSwitch()}
          />

          <Text style={[styles.text]}>غیر فعال</Text>
        </View>
        <Text style={[styles.text, {paddingRight: '5%'}]}>نوتیفیکیشن</Text>
      </View>
      <View style={styles.box}>
        <View style={styles.taggle}>
          <ToggleSwitch
            isOn={isEnabled2}
            onColor={colors.blue}
            offColor={colors.white}
            labelStyle={{color: 'black'}}
            size="medium"
            onToggle={() => toggleSwitch2()}
          />

          <Text style={[styles.text]}>غیر فعال</Text>
        </View>
        <Text style={[styles.text, {paddingRight: '5%'}]}>حالت تاریک</Text>
      </View>
      <View
            style={{
              marginTop: '5%',
              height: '8%',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              position:'absolute',
              bottom:0
            }}>
            <TouchableOpacity style={styles.icon}>
              <Setting name="setting" color={colors.darkgreenblue} size={25} />
            </TouchableOpacity>
            <ButtonComponent
              buttonStyle={{
                borderRadius: 10,
                backgroundColor: colors.white,
                borderColor: colors.blue,
                alignSelf: 'center',
                width: '70%',
              }}
              onClick={() => btn()}
              titleStyle={{color: colors.blue,fontSize: 18}}
              title={'مشاهده لیست و تجهیزات '}
            />
            <TouchableOpacity
              style={[
                styles.icon,
                {borderTopLeftRadius: 30, borderTopRightRadius: 0},
              ]}>
              <Notti
                name="ios-notifications-outline"
                color={colors.darkgreenblue}
                size={25}
              />
            </TouchableOpacity>
          </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.blue,
    fontSize: 16,
  },

  box: {
    width: '95%',
    height: '8%',
    backgroundColor: colors.lightwhite,
    alignSelf: 'center',
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop:'5%'
  },

  icon: {
    borderTopRightRadius: 30,
    width: 50,
    height: 50,
    backgroundColor: colors.lightwhite,
    justifyContent: 'center',
    alignItems: 'center',
  },
  taggle: {
    width: '30%',
    flexDirection: 'row',
    marginLeft: '5%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default Seting;
