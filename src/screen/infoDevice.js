import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  TouchableOpacity,
  FlatList,
  Modal
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Idcard from 'react-native-vector-icons/AntDesign';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Icon from 'react-native-vector-icons/FontAwesome';
import NetInfo from '@react-native-community/netinfo';
import HeaderComponent from '../component/headerComponent';
import InfoDeviceComponent from '../component/InfoDeviceComponent';
// import Modal from 'react-native-modal';
import InfoDeviceModal from '../ModalComponent/infoDeviceModal';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const InfoDevice = (props) => {
  const [isModalVisible, setModalVisible] = useState(false);

  const data = [
    {
      id: 1,
      name: 'اطلاعات اولیه دستگاه',
      code: '340290',
      deviceCode: '2439489038',
      model: 'مدل سی سی ا',
      made: 'فراوان',
      status: 'فعال',
    },
    {
      id: 2,
      name: 'اطلاعات اولیه دستگاه',
      code: '340290',
      deviceCode: '2439489038',
      model: 'مدل سی سی ا',
      made: 'فراوان',
      status: 'فعال',
    },
  ];

  const btn = () => {
    setModalVisible(true);
  };
  return (
    <View style={[styles.container]}>
      <Modal
        visible={isModalVisible}
        animationType='fade'
        transparent={true}
        // style={{
        //   backgroundColor: colors.black,
        //   width: '100%',
        //   alignSelf: 'center',
        //   height: HEIGHT,
        // }}
        >
        <InfoDeviceModal />
      </Modal>
      <HeaderComponent colorBackIcon={'#1879a4'} />
      <View style={styles.line}>
        <View style={styles.linechield}></View>
      </View>
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={styles.main}>
          <View style={styles.profile}>
            <View>
              <Text style={[styles.text, {color: colors.blue, fontSize: 22}]}>
                نام دستگاه : مانومتر
              </Text>
              <Text
                style={[styles.text, {color: colors.gray, paddingRight: '8%'}]}>
                دسته بندی : تردمیل
              </Text>
            </View>
            <View style={styles.profileImage}></View>
          </View>
          <FlatList
            data={data}
            renderItem={({item}) => <InfoDeviceComponent item={item} />}
          />
        </View>

        <ButtonComponent
          buttonStyle={{
            borderRadius: 10,
            backgroundColor: colors.blue,
            borderColor: colors.blue,
            alignSelf: 'center',
            marginTop: '10%',
          }}
          onClick={() => btn()}
          titleStyle={{fontSize: 16}}
          titleStyle={{color: colors.white}}
          title={'تایید و اضافه کردن'}
        />
        <ButtonComponent
          buttonStyle={{
            borderRadius: 10,
            backgroundColor: colors.white,
            borderColor: colors.blue,
            alignSelf: 'center',
          }}
          onClick={() => btn()}
          titleStyle={{fontSize: 16}}
          titleStyle={{color: colors.blue}}
          title={'مشاهده لیست تجهیزات'}
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightwhite,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.navyblue,
    fontSize: 16,
    paddingRight: '3%',
  },

  line: {
    width: '100%',
    height: 10,
    backgroundColor: '#dce3ed',
  },
  linechield: {
    width: '20%',
    height: 10,
    backgroundColor: colors.blue,
  },
  profile: {
    width: '100%',
    height: HEIGHT / 6,
    // backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  main: {
    // backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: '8%',
    width: '95%',
    alignSelf: 'center',
  },
  profileImage: {
    width: '30%',
    height: '100%',
    backgroundColor: colors.white,
    borderRadius: 10,
  },
});

export default InfoDevice;
