import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import Idcard from 'react-native-vector-icons/AntDesign';
import Mobile from 'react-native-vector-icons/FontAwesome';
import allStyle from '../utils/allStyle';
import InputComponent from '../component/inputComponent';
import {validate} from '../utils/validation';
import Icon from 'react-native-vector-icons/FontAwesome';
import NetInfo from '@react-native-community/netinfo';
import HeaderComponent from '../component/headerComponent';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const Search = (props) => {
  // props.setloading(false);
  const [devicename, setdevicename] = useState();
  const [deviceMessage, setdeviceMessage] = useState();

  const [UMDNSname, setUMDNSname] = useState();
  const [UMDNSMessage, setUMDNSMessage] = useState();

  const [CodeDevice, setCodeDevice] = useState();
  const [CodeDeviceMessage, setCodeDeviceMessage] = useState();

  const [net, setNet] = useState();
  const [error, setError] = useState();

  //   const checkCode = () => {
  //     NetInfo.addEventListener((state) => {
  //       if (state.isConnected) {
  //         // setTimeout(() => {
  //         //     props.navigation.navigate('Index')
  //         // },4000);
  //         setNet(true);
  //         // props.navigation.navigate('Index')

  //         fetch('http://37.152.181.14:300/api/otp/start', {
  //           method: 'POST',
  //           // headers: {
  //           //   Accept: 'application/json',
  //           //   'Content-Type': 'application/json',
  //           // },
  //           body: JSON.stringify({mobileNo: phone}),
  //         })
  //           .then((response) => response.json())
  //           // if (res.status === 'success') {
  //           //     console.warn(res)
  //           //   return  props.navigation.navigate('Index')
  //           .then((responseJson) => {
  //             if (responseJson.status == 'success') {
  //               // console.warn(responseJson);
  //               // AsyncStorage.setItem('login', JSON.stringify(1));
  //               props.navigation.navigate('OtpLogin');
  //             } else {
  //               setError(responseJson.message[0]);
  //             }
  //           });
  //       } else {
  //         setNet(false);
  //         setError('لطفا ابتدا اینترنت خود را روشن کنید سپس مجددا تلاش کنید');
  //       }
  //     });
  //   };

  const arrayValue = [
    {
      id: 1,
      nameField: ['devicename'],
      value: devicename == undefined ? '' : devicename,
    },
    {
      id: 2,
      nameField: ['UMDNSname'],
      value: UMDNSname == undefined ? '' : UMDNSname,
    },
    {
      id: 3,
      nameField: ['CodeDevice'],
      value: CodeDevice == undefined ? '' : CodeDevice,
    },
  ];

  const checkValue = () => {
    let all = true;
    arrayValue.map((item) => {
      let id = item.id,
        value = item.value,
        nameField = item.nameField;
      let resp = [null, null];
      nameField.map((item) => {
        let v = validate(item, value, 'en');
        let checkValidation = v[0];
        let checkValidateMessage = v[1];
        resp[0] = resp[0] || checkValidation ? true : false; // validate is true or false
        resp[1] = checkValidateMessage; // message error
      });
      if (resp[0]) {
        if (id === 1) {
          // devicename
        } else if (id === 2) {
          // UMDNSname
        } else {
          //Codedevice
        }
      } else {
        all = false;
        if (id === 1) {
          // devicename
          setdeviceMessage(resp[1]);
        } else if (id === 2) {
          // UMDNSname
          setUMDNSMessage(resp[1]);
        } else {
          // Codedevice
          setCodeDeviceMessage(resp[1]);
        }
      }
    });

    return all;
  };

  const btn = () => {
    if (checkValue()) {
      //   checkCode()
    }
  };
  return (
    <View style={[styles.container]}>
      <HeaderComponent colorBackIcon={'#1879a4'}/>
      <View style={{width:'100%',height:10,backgroundColor:'#dce3ed'}}>
          <View style={{width:'40%',height:10,backgroundColor:colors.blue}}></View>
      </View>
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={styles.box}>
          <View style={styles.textbox}>
            <Text style={[styles.text, {fontSize: 25}]}>پیدا کردن دستگاه</Text>
            <Text style={[styles.text, {fontSize: 18}]}>
              در این بخش دستگاه مورد نظز خود را پیدا کنید
            </Text>
          </View>

          <InputComponent
            containertotal={{marginTop: '5%'}}
            lable="نام دستگاه"
            stylelable={{fontSize: 18}}
            placeholder={'نام دستگاه'}
            keyboardType={'numeric'}
            maxLength={11}
            onChangeInput={(value) => setdevicename(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 2,
              borderRadius: 8,
              height: 55,
              borderColor: colors.blue,
              width: '90%',
              backgroundColor: colors.white,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            errorMessage={deviceMessage}
            input={{textAlign: 'right', fontSize: 18}}
            eror={{paddingRight: '5%'}}
          />
          <InputComponent
            lable="نام UMDNS"
            stylelable={{fontSize: 18}}
            placeholder={'نام UMDNS'}
            keyboardType={'numeric'}
            maxLength={11}
            onChangeInput={(value) => setUMDNSname(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 2,
              borderRadius: 8,
              height: 55,
              borderColor: colors.blue,
              width: '90%',
              backgroundColor: colors.white,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            errorMessage={UMDNSMessage}
            input={{textAlign: 'right', fontSize: 18}}
            eror={{paddingRight: '5%'}}
          />
          <InputComponent
            lable="کد دستگاه"
            stylelable={{fontSize: 18}}
            placeholder={'کد دستگاه'}
            keyboardType={'numeric'}
            maxLength={11}
            onChangeInput={(value) => setCodeDevice(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 2,
              borderRadius: 8,
              height: 55,
              borderColor: colors.blue,
              width: '90%',
              backgroundColor: colors.white,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            errorMessage={CodeDeviceMessage}
            input={{textAlign: 'right', fontSize: 18}}
            eror={{paddingRight: '5%'}}
          />
          <ButtonComponent
            buttonStyle={{
              borderRadius: 10,
              backgroundColor: colors.blue,
              borderColor: colors.blue,
              alignSelf: 'center',
            }}
            onClick={() => btn()}
            titleStyle={{fontSize: 16}}
            titleStyle={{color: colors.white}}
            title={'ورود'}
          />
          <View
            style={{
              alignSelf: 'center',
              width: '100%',
              height: 50,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '40%',
                height: 2,
                backgroundColor: colors.lightblue,
              }}></View>
            <Text style={[styles.text, {colors: colors.lightblue}]}>
              QRCODE
            </Text>
            <View
              style={{
                width: '40%',
                height: 2,
                backgroundColor: colors.lightblue,
              }}></View>
          </View>
          <TouchableOpacity>
            <Image
              source={require('../images/qrcode.png')}
              style={{
                width: 100,
                height: 100,
                alignSelf: 'center',
              }}
            />
          </TouchableOpacity>
           <Text style={[styles.text,{textAlign:'center',fontSize:18}]}>scan</Text>

        </View>
        
        <ButtonComponent
          buttonStyle={{
            width:'80%',
            borderRadius: 10,
            backgroundColor: colors.white,
            borderColor: colors.blue,
            alignSelf: 'center',
          }}
          onClick={() => btn()}
          titleStyle={{fontSize: 16}}
          titleStyle={{color: colors.blue}}
          title={'مشاهده لیست تجهیزات'}
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightwhite,
    flex: 1,
  },

  title: {
    ...allStyle.textBold,
    color: colors.gray,
    fontSize: 16,
  },
  text: {
    ...allStyle.text,
    color: colors.navyblue,
    fontSize: 16,
  },
  box: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: '8%',
    width: '95%',
    alignSelf: 'center',
  },
  register: {
    // backgroundColor:'red',

    marginTop: '5%',
    alignSelf: 'center',
    width: '65%',
  },

  box1: {
    padding: 20,
    backgroundColor: colors.green,
    shadowColor: '#c5eff4',
    shadowOpacity: 0.8,
    shadowRadius: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },

    width: 130,
    height: 130,
    elevation: 5,
    borderRadius: 100,
    // borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    width: '95%',
    marginTop: '5%',
  },
});

export default Search;
