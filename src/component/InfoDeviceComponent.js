import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import allStyle from '../utils/allStyle';
import {validate} from '../utils/validation';


const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const InfoDevice = (props) => {

 

  
const item = props.item
  const btn = () => {
    if (checkValue()) {
      //   checkCode()
    }
  };
  return (
    <View style={[styles.container]}>
      <Text style={styles.title}>{item.name}</Text>
      <View style={{flexDirection:'row',justifyContent:'space-between'}}>
      <Text style={[styles.text,{paddingLeft:'3%'}]}>کد : {item.deviceCode}</Text>
         <Text style={[styles.text,{paddingRight:'3%'}]}>کد : {item.code}</Text>
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-between'}}>
      <Text style={[styles.text,{paddingLeft:'16%'}]}>کد : {item.made}</Text>
         <Text style={[styles.text,{paddingRight:'3%'}]}>مدل : {item.model}</Text>
      </View>
      <Text style={[styles.text,{paddingRight:'3%'}]}>وضعیت : {item.status}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    marginTop:'5%',
    borderRadius:8,
    height:HEIGHT/3.8
  },

  title: {
    ...allStyle.textBold,
    color: colors.blue,
    fontSize: 20,
    paddingRight:'3%',
    marginTop:'3%'

  },
  text: {
    ...allStyle.text,
    color: colors.blue,
    fontSize: 18,
    marginTop:'3%'
  },
  box: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: '8%',
    width: '95%',
    alignSelf: 'center',
  },
  register: {
    // backgroundColor:'red',

    marginTop: '5%',
    alignSelf: 'center',
    width: '65%',
  },

  box1: {
    padding: 20,
    backgroundColor: colors.green,
    shadowColor: '#c5eff4',
    shadowOpacity: 0.8,
    shadowRadius: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },

    width: 130,
    height: 130,
    elevation: 5,
    borderRadius: 100,
    // borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    width: '95%',
    marginTop: '5%',
  },
});

export default InfoDevice;
