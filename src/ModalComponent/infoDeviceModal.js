import React, {useEffect, useState} from 'react';
import {
  Image,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Text,
  Modal,
  TouchableOpacity,
} from 'react-native';
import colors from '../utils/colors';
import ButtonComponent from '../component/buttonComponent';
import allStyle from '../utils/allStyle';
import {validate} from '../utils/validation';
import InputComponent from '../component/inputComponent';
import {RadioButton} from 'react-native-paper';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const InfoDeviceModal = (props) => {
  const [checked, setChecked] = useState('');

  const check = (value) => {
    console.warn(value);
    setChecked(value);
  };

  const item = props.item;
  const btn = () => {
    if (checkValue()) {
      //   checkCode()
    }
  };
  return (
    <View style={[styles.container]}>
      <View style={styles.modal}>
        <ScrollView keyboardShouldPersistTaps={'handled'}>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <InputComponent
              containertotal={{marginTop: '3%'}}
              placeholdercolor={colors.lightblue}
              lable="هزینه"
              stylelable={{fontSize: 16}}
              placeholder={'هزینه'}
              keyboardType={'numeric'}
              maxLength={11}
              onChangeInput={(value) => setphone(value)}
              style={{
                alignSelf: 'center',
                borderWidth: 1,
                borderRadius: 8,
                height: 55,
                borderColor: colors.gray,
                width: '90%',
                backgroundColor: colors.white,
              }}
              stylelable={{color: colors.gray, textAlign: 'right'}}
              // errorMessage={PhoneMessage}
              input={{textAlign: 'right', fontSize: 18}}
              eror={{paddingRight: '5%'}}
            />
            <InputComponent
              containertotal={{marginTop: '3%'}}
              lable="ref.no"
              stylelable={{fontSize: 16}}
              placeholder={'ref.no'}
              placeholdercolor={colors.lightblue}
              keyboardType={'numeric'}
              maxLength={11}
              onChangeInput={(value) => console.warn(value)}
              style={{
                alignSelf: 'center',
                borderWidth: 1,
                borderRadius: 8,
                height: 55,
                borderColor: colors.gray,
                width: '90%',
                backgroundColor: colors.white,
              }}
              stylelable={{color: colors.gray, textAlign: 'right'}}
              // errorMessage={PhoneMessage}
              input={{textAlign: 'right', fontSize: 18}}
              eror={{paddingRight: '5%'}}
            />
          </View>
          <InputComponent
            lable="زمان کالیبراسیون"
            lableStyle={{fontSize: 16, paddingRight: 0}}
            placeholder={'زمان کالیبراسیون'}
            placeholdercolor={colors.lightblue}
            keyboardType={'numeric'}
            maxLength={11}
            onChangeInput={(value) => console.warn(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 1,
              borderRadius: 8,
              height: 55,
              borderColor: colors.gray,
              width: '95%',
              backgroundColor: colors.white,
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            // errorMessage={PhoneMessage}
            input={{textAlign: 'right', fontSize: 18}}
            eror={{paddingRight: '5%'}}
          />

          <View style={styles.status}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <RadioButton
                color={colors.blue}
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => check('first')}
              />
              <Text style={styles.text}>رد</Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <RadioButton
                color={colors.blue}
                value="first"
                status={checked === 'secend' ? 'checked' : 'unchecked'}
                onPress={() => check('secend')}
              />
              <Text style={styles.text}>شرطی</Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <RadioButton
                color={colors.blue}
                value="first"
                status={checked === 'third' ? 'checked' : 'unchecked'}
                onPress={() => check('third')}
              />
              <Text style={styles.text}>قبول</Text>
            </View>
            <Text style={[styles.text, {marginTop: 0, color: colors.darkGray}]}>
              وضعیت
            </Text>
          </View>
          <TouchableOpacity style={styles.attash}>
            <Text style={[styles.text, {marginTop: 0}]}>
              attachment document
            </Text>
          </TouchableOpacity>
          <InputComponent
            lable="زمان کالیبراسیون"
            lableStyle={{fontSize: 16, paddingRight: 0}}
            placeholder={'زمان کالیبراسیون'}
            placeholdercolor={colors.lightblue}
            keyboardType={'default'}
            maxLength={300}
            onChangeInput={(value) => console.warn(value)}
            style={{
              alignSelf: 'center',
              borderWidth: 1,
              borderRadius: 8,
              borderColor: colors.gray,
              width: '95%',
              backgroundColor: colors.white,
              height: HEIGHT / 4,
            }}
            containertotal={{
              backgroundColor: colors.white,
              height: HEIGHT / 3,
              marginTop: '3%',
            }}
            stylelable={{color: colors.gray, textAlign: 'right'}}
            // errorMessage={PhoneMessage}
            input={{textAlign: 'right', fontSize: 18}}
            eror={{paddingRight: '5%'}}
          />
          <View style={styles.line}></View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
              height: HEIGHT / 6,
            }}>
            <ButtonComponent
              buttonStyle={{
                marginTop: 0,
                borderRadius: 10,
                backgroundColor: colors.white,
                borderColor: colors.white,
                alignSelf: 'center',
                width: '40%',
              }}
              onClick={() => btn()}
              titleStyle={{fontSize: 16}}
              titleStyle={{color: colors.blue}}
              title={'کنسل کردن'}
            />
            <ButtonComponent
              buttonStyle={{
                marginTop: 0,
                width: '40%',
                borderRadius: 10,
                backgroundColor: colors.white,
                borderColor: colors.blue,
                alignSelf: 'center',
              }}
              onClick={() => btn()}
              titleStyle={{fontSize: 16}}
              titleStyle={{color: colors.blue}}
              title={'اضافه کردن'}
            />
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightblack,
    flex: 1,
  },
  modal: {
    backgroundColor: colors.white,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,

    marginTop: HEIGHT / 3,
    width: '100%',
    zIndex: 2,
    opacity: 1,
  },
  title: {
    ...allStyle.textBold,
    color: colors.blue,
    fontSize: 20,
    paddingRight: '3%',
    marginTop: '3%',
  },
  text: {
    ...allStyle.text,
    color: colors.lightblue,
    fontSize: 18,
    marginTop: '3%',
  },
  box: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginTop: '8%',
    width: '95%',
    alignSelf: 'center',
  },
  register: {
    // backgroundColor:'red',

    marginTop: '5%',
    alignSelf: 'center',
    width: '65%',
  },

  box1: {
    padding: 20,
    backgroundColor: colors.green,
    shadowColor: '#c5eff4',
    shadowOpacity: 0.8,
    shadowRadius: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },

    width: 130,
    height: 130,
    elevation: 5,
    borderRadius: 100,
    // borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbox: {
    width: '95%',
    marginTop: '5%',
  },
  attash: {
    borderStyle: 'dashed',
    borderWidth: 1,
    height: '8%',
    width: '95%',
    alignSelf: 'center',
    borderColor: colors.gray,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
  },
  status: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '90%',
    marginTop: '3%',
  },
  line: {
    width: '100%',
    height: 2,
    backgroundColor: colors.blue,
  },
});

export default InfoDeviceModal;
